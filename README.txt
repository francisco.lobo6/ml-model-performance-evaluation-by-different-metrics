A Python class for the evaluation of ML model performance. The result is scalable, since it provides results based on all the sklearn scorers that the user specifies. The class also implements an estimation of yearly scientific articles referring to a particular scorer.

The Python class (performance_evaluation) receives two input variables:
i)a list of predictions from different models for a target variable,
and ii)the actual target variable values.


The main method in the class is predictions_assessment. To invoke this method it is necessary to provide a yaml configuration file. An example of this file is given in this directory (input_example.yaml). The configuration file indicates the sklearn scorers along with the parameters to use for each scorer. The method predictions_assessment will carry out evaluation of each model prediction with each of the sklearn scorers specified in the yaml file. More precisely, the scorers' scoring function will be invoked using the parameters specified in the yaml file.
In this manner, the program is easily scalable since the user only needs to add more sklearn scorers and associated parameters as required.
The result is a data frame containing the different scorings, where the rows represent the models and the columns the scoring function values.

NOTE: For the generation of the configuration yaml file another method of the class is useful, in particular, the generate_default_config_scorers method. This method generates a yaml file comprising all sklearner scorers with their default input parameters. From this yaml file, it is easy for the user to copy and paste the specific scorers needed to create the configuration file, and modify the parameters as required.
Nevertheless, it is the responsability of the user to include scorers in the configuation file which are coherent with the type of prediction and target data. For example, if it is a classification problem it does not make sense to use the r2_score scoring function.
The generate_default_config_scorers in turn employs an auxiliary method in the class named obtain_scorers_url_name, which deals with differences found between the scorer name and the associated url documentation to extract the default parameters.


The other main method of the class is scorer_usage, which provides a yearly estimation of the number of scientific articles that cite/employ a particular scorer. This is done employing CrossRef API, which includes over 75 million published works. Currently, the CrossRef API does not allow for literal sentence search (only by words), so the result is just an approximate estimation of the scorer usage tendency over the years. The user needs to provide a scorer name and a range of years to invoke this method.
